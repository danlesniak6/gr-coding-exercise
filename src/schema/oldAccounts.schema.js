module.exports = (db) => {
  const Account = db.old.define("accounts", {
    id: {
      primaryKey: true,
      type: db.Sequelize.STRING
    },
    name: {
      type: db.Sequelize.STRING
    },
    email: {
      type: db.Sequelize.STRING
    }
  })

  return Account
}