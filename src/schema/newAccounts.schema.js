module.exports = (db) => {
  const Account = db.new.define("accounts", {
    id: {
      primaryKey: true,
      type: db.Sequelize.STRING
    },
    name: {
      type: db.Sequelize.STRING
    },
    email: {
      type: db.Sequelize.STRING
    },
    favoriteFlavor: {
      type: db.Sequelize.STRING,
      field: 'favorite_flavor'
    }
  })

  return Account
}