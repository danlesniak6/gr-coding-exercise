const Sequelize = require("sequelize")
const oldDBConfig = require("../config/oldDB.config")
const newDBConfig = require("../config/newDB.config")

const oldDB = new Sequelize(oldDBConfig.DB, oldDBConfig.USER, oldDBConfig.PASSWORD, {
  host: oldDBConfig.HOST,
  port: oldDBConfig.port,
  dialect: oldDBConfig.dialect,
  operatorsAliases: 0,
  define: {
    timestamps: false
  },
  logging: false
})

const newDB = new Sequelize(newDBConfig.DB, newDBConfig.USER, newDBConfig.PASSWORD, {
  host: newDBConfig.HOST,
  port: newDBConfig.port,
  dialect: newDBConfig.dialect,
  operatorsAliases: 0,
  define: {
    timestamps: false
  },
  logging: false
})

const db = {
  Sequelize,
  old: oldDB,
  new: newDB
}

db.oldAccounts = require("./oldAccounts.schema.js")(db)
db.newAccounts = require("./newAccounts.schema.js")(db)

module.exports = db