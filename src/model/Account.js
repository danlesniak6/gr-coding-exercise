const AccountMigrationError = require('./AccountMigrationError')

class Account {
    id
    name
    email

    constructor(account) {
        for (let prop in account) {
            this[prop] = account[prop]
        }
    }
}

const AccountErrorTypes = Object.freeze({
    MISSED: 1,
    ADDED: 2,
    CORRUPTED: 3
})

module.exports = {
    AccountErrorTypes,
    OldAccount: class extends Account {
        constructor(account) {
            super(account)
        }

        findMigrationError = (matchingAccount) => {
            let migrationError = null
            if (!matchingAccount) {
                migrationError = new AccountMigrationError(this.id, AccountErrorTypes.MISSED)
            }
            return migrationError
        }
    },
    NewAccount: class extends Account {
        favoriteFlavor

        constructor(account) {
            super(account)
        }

        findMigrationError = (matchingAccount) => {
            let migrationError = null
            if (!matchingAccount) {
                migrationError = new AccountMigrationError(this.id, AccountErrorTypes.ADDED)
            } else {
                if (matchingAccount.name != this.name || matchingAccount.email != this.email) {
                    const corruptions = []
                    if (matchingAccount.name != this.name) {
                        corruptions.push({
                            field: 'name',
                            oldValue: matchingAccount.name,
                            newValue: this.name
                        })
                    }
                    if (matchingAccount.email != this.email) {
                        corruptions.push({
                            field: 'email',
                            oldValue: matchingAccount.email,
                            newValue: this.email
                        })
                    }
                    migrationError = new AccountMigrationError(this.id, AccountErrorTypes.CORRUPTED, corruptions)
                }
            }
            return migrationError
        }
    }
}


