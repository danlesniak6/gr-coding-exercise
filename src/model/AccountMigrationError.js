module.exports = class AccountMigrationError {
    accountId
    errorType
    corruptions = []

    constructor(accountId, errorType, corruptions) {
        this.accountId = accountId
        this.errorType = errorType
        this.corruptions = corruptions
    }
}