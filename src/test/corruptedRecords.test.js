const assert = require("assert")
const { getMigrationErrors } = require("../util/report.util")
const { OldAccount, NewAccount, AccountErrorTypes } = require('../model/Account')

describe("Corrupted Records", () => {
    let oldAccounts = null
    let newAccounts = null
    let migrationErrors = null
    beforeEach(() => {
        const oldAccountsData = require("./data/premigrationTestData.json")
        const newAccountsData = require("./data/postmigrationTestData.json")
        oldAccounts = oldAccountsData.map(a => new OldAccount(a))
        newAccounts = newAccountsData.map(a => new NewAccount(a))
    });
    describe("Zero Corrupted Records", () => {
        beforeEach(() => {
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
        });
        it("should yield zero corrupted records", () => {
            assert.equal(migrationErrors.length, 0)
        })
    })
    describe("One Corrupted Record with One Corrupt Field", () => {
        beforeEach(() => {
            newAccounts[0].name = 'James'
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
        });
        it("should yield one corrupted record with correct accountId", () => {
            assert.equal(migrationErrors.length, 1)
            assert.equal(migrationErrors[0].accountId, newAccounts[0].id)
            assert.equal(migrationErrors[0].errorType, AccountErrorTypes.CORRUPTED)
        })
        it("should yield one corrupted record with corrupt 'name' field", () => {
            const corruptions = migrationErrors[0].corruptions
            assert.equal(corruptions.length, 1)
            assert.equal(corruptions[0].field, 'name')
        })
        it("should store correct old and new values", () => {
            const corruptions = migrationErrors[0].corruptions
            const oldAccount = oldAccounts.find(a => a.id == migrationErrors[0].accountId)
            assert.equal(corruptions[0].oldValue, oldAccount.name)
            assert.equal(corruptions[0].newValue, newAccounts[0].name)
        })
    })
    describe("One Corrupted Record with Two Corrupt Fields", () => {
        beforeEach(() => {
            newAccounts[0].name = 'James'
            newAccounts[0].email = 'OmegaJames@gmail.com'
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
        });
        it("should yield one corrupted record with correct accountId", () => {
            assert.equal(migrationErrors.length, 1)
            assert.equal(migrationErrors[0].accountId, newAccounts[0].id)
            assert.equal(migrationErrors[0].errorType, AccountErrorTypes.CORRUPTED)
        })
        it("should yield one corrupted record with corrupt 'name' and 'email' fields", () => {
            const corruptions = migrationErrors[0].corruptions
            assert.equal(corruptions.length, 2)
            assert.equal(corruptions.some(c => c.field == 'name'), true)
            assert.equal(corruptions.some(c => c.field == 'email'), true)

        })
        it("should store correct old and new values", () => {
            const corruptions = migrationErrors[0].corruptions
            const oldAccount = oldAccounts.find(a => a.id == migrationErrors[0].accountId)
            assert.equal(corruptions.find(c => c.field == 'name').oldValue, oldAccount.name)
            assert.equal(corruptions.find(c => c.field == 'name').newValue, newAccounts[0].name)
            assert.equal(corruptions.find(c => c.field == 'email').oldValue, oldAccount.email)
            assert.equal(corruptions.find(c => c.field == 'email').newValue, newAccounts[0].email)
        })
    })
    describe("Many Corrupted Records with Many Corrupt Fields", () => {
        beforeEach(() => {
            newAccounts[0].name = 'James'
            newAccounts[3].name = 'Nadja'
            newAccounts[8].name = 'Horrus'
            newAccounts[8].email = 'HorrusRocks@hotmail.com'
            newAccounts[12].name = 'Albert'
            newAccounts[14].email = 'Mauriceosarus@gmail.com'
            newAccounts[17].name = 'Louis'
            newAccounts[17].email = 'beachboy44@gmail.com'

            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
        });
        it("should yield 6 corrupted records with correct accountIds", () => {
            assert.equal(migrationErrors.length, 6)
            assert.equal(migrationErrors.every(e => e.errorType == AccountErrorTypes.CORRUPTED), true)
            const accountIds = migrationErrors.map(e => e.accountId)
            const allIdsIncluded = accountIds.includes(newAccounts[0].id)
                && accountIds.includes(newAccounts[3].id)
                && accountIds.includes(newAccounts[8].id)
                && accountIds.includes(newAccounts[12].id)
                && accountIds.includes(newAccounts[14].id)
                && accountIds.includes(newAccounts[17].id)
            assert.equal(allIdsIncluded, true)
        })
        it("should yield corrupted records with 5 'name' and 3 'email' fields corrupted", () => {
            const numNameCorruptions = migrationErrors.map(e => e.corruptions).flat().filter(c => c.field == 'name').length
            const numEmailCorruptions = migrationErrors.map(e => e.corruptions).flat().filter(c => c.field == 'email').length
            assert.equal(numNameCorruptions, 5)
            assert.equal(numEmailCorruptions, 3)
        })
        it("should store correct old and new values", () => {
            const alteredIndicies = [0, 3, 8, 12, 14, 17]
            alteredIndicies.forEach(i => {
                const migrationError = migrationErrors.find(e => e.accountId == newAccounts[i].id)
                const oldAccount = oldAccounts.find(a => a.id == migrationError.accountId)
                migrationError.corruptions.forEach(c => {
                    assert.equal(c.oldValue, oldAccount[c.field])
                    assert.equal(c.newValue, newAccounts[i][c.field])
                })

            })
        })
    })
})