const assert = require("assert")
const { getMigrationErrors, generateMigrationReport } = require("../util/report.util")
const { OldAccount, NewAccount, AccountErrorTypes } = require('../model/Account')

// These are kind of garbage tests since they depend on the specific verbiage of the report
// but I included them for thoroughness
describe("Report Generator", () => {
    let oldAccounts = null
    let newAccounts = null
    let migrationErrors = null
    let migrationReport = null
    beforeEach(() => {
        const oldAccountsData = require("./data/premigrationTestData.json")
        const newAccountsData = require("./data/postmigrationTestData.json")
        oldAccounts = oldAccountsData.map(a => new OldAccount(a))
        newAccounts = newAccountsData.map(a => new NewAccount(a))
    });
    describe("Zero migration Errors", () => {
        beforeEach(() => {
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
            migrationReport = generateMigrationReport(migrationErrors, true)
        });
        it("should yield zero migration errors", () => {
            assert.equal(migrationErrors.length, 0)
        })
        it("should show zero migration errors in report", () => {
            assert.equal(!migrationReport.includes('was missed'), true)
            assert.equal(!migrationReport.includes('was added'), true)
            assert.equal(!migrationReport.includes('was corrupted'), true)
        })
    })
    describe("One Missed Record", () => {
        beforeEach(() => {
            newAccounts.splice(0, 1)
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
            migrationReport = generateMigrationReport(migrationErrors, true)
        });
        it("should yield one missed record with correct accountId", () => {
            assert.equal(migrationErrors.length, 1)
            assert.equal(migrationErrors[0].errorType, AccountErrorTypes.MISSED)
            assert.equal(migrationErrors[0].accountId, oldAccounts[0].id)
        })
        it("should show only one missed error in report with correct accountId", () => {
            assert.equal(!migrationReport.includes('was added'), true)
            assert.equal(!migrationReport.includes('was corrupted'), true)
            const missedCount = (migrationReport.match(/was missed/g) || []).length
            assert.equal(missedCount, 1)
            assert.equal(migrationReport.includes(oldAccounts[0].id), true)
        })
    })
    describe("One Added Record", () => {
        beforeEach(() => {
            oldAccounts.splice(0, 1)
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
            migrationReport = generateMigrationReport(migrationErrors, true)
        });
        it("should yield one added record with correct accountId", () => {
            assert.equal(migrationErrors.length, 1)
            assert.equal(migrationErrors[0].errorType, AccountErrorTypes.ADDED)
            assert.equal(migrationErrors[0].accountId, newAccounts[0].id)
        })
        it("should show only one added error in report with correct accountId", () => {
            assert.equal(!migrationReport.includes('was missed'), true)
            assert.equal(!migrationReport.includes('was corrupted'), true)
            const addedCount = (migrationReport.match(/was added/g) || []).length
            assert.equal(addedCount, 1)
            assert.equal(migrationReport.includes(newAccounts[0].id), true)
        })
    })
    describe("One Corrupted Record", () => {
        beforeEach(() => {
            newAccounts[0].name = 'James'
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
            migrationReport = generateMigrationReport(migrationErrors, true)
        });
        it("should yield one corrupted record with correct accountId", () => {
            assert.equal(migrationErrors.length, 1)
            assert.equal(migrationErrors[0].accountId, newAccounts[0].id)
            assert.equal(migrationErrors[0].errorType, AccountErrorTypes.CORRUPTED)
        })
        it("should show only one corrupted error in report with correct accountId", () => {
            assert.equal(!migrationReport.includes('was missed'), true)
            assert.equal(!migrationReport.includes('was added'), true)
            const corruptedCount = (migrationReport.match(/was corrupted/g) || []).length
            assert.equal(corruptedCount, 1)
            assert.equal(migrationReport.includes(newAccounts[0].id), true)
        })
    })
})