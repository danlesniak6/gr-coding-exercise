const assert = require("assert")
const { getMigrationErrors } = require("../util/report.util")
const { OldAccount, NewAccount, AccountErrorTypes } = require('../model/Account')

describe("Added Records", () => {
    let oldAccounts = null
    let newAccounts = null
    let migrationErrors = null
    beforeEach(() => {
        const oldAccountsData = require("./data/premigrationTestData.json")
        const newAccountsData = require("./data/postmigrationTestData.json")
        oldAccounts = oldAccountsData.map(a => new OldAccount(a))
        newAccounts = newAccountsData.map(a => new NewAccount(a))
    });
    describe("Zero Added Records", () => {
        beforeEach(() => {
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
        });
        it("should yield zero corrupted records", () => {
            assert.equal(migrationErrors.length, 0)
        })
    })
    describe("One Added Record", () => {
        beforeEach(() => {
            oldAccounts.splice(0, 1)
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
        });
        it("should yield one added record with correct accountId", () => {
            assert.equal(migrationErrors.length, 1)
            assert.equal(migrationErrors[0].errorType, AccountErrorTypes.ADDED)
            assert.equal(migrationErrors[0].accountId, newAccounts[0].id)
        })
    })
    describe("Many Added Records", () => {
        beforeEach(() => {
            oldAccounts.splice(0, 6)
            migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
        });
        it("should yield 6 added records with correct accountIds", () => {
            assert.equal(migrationErrors.length, 6)
            assert.equal(migrationErrors.every(e => e.errorType == AccountErrorTypes.ADDED), true)
            const accountIds = migrationErrors.map(e => e.accountId)
            const allIdsIncluded = accountIds.includes(newAccounts[0].id)
                && accountIds.includes(newAccounts[1].id)
                && accountIds.includes(newAccounts[2].id)
                && accountIds.includes(newAccounts[3].id)
                && accountIds.includes(newAccounts[4].id)
                && accountIds.includes(newAccounts[5].id)
            assert.equal(allIdsIncluded, true)
        })
    })
})