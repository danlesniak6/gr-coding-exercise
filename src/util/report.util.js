const { AccountErrorTypes } = require('../model/Account')

module.exports = {
    getMigrationErrors: (oldAccounts, newAccounts) => {
        const migrationErrors = []
        const maxRecords = Math.max(...[oldAccounts.length, newAccounts.length])
        for (let i = 0; i < maxRecords; i++) {
            if (oldAccounts[i]) {
                const oldAccount = oldAccounts[i]
                const matchingAccount = newAccounts.find(newAccount => newAccount.id == oldAccount.id)
                const migrationError = oldAccount.findMigrationError(matchingAccount)
                if(migrationError) {
                    migrationErrors.push(migrationError)
                }
            }
            if (newAccounts[i]) {
                const newAccount = newAccounts[i]
                const matchingAccount = oldAccounts.find(oldAccount => oldAccount.id == newAccount.id)
                const migrationError = newAccount.findMigrationError(matchingAccount)
                if(migrationError) {
                    migrationErrors.push(migrationError)
                }
            }
        }
        return migrationErrors
    },
    generateMigrationReport: (migrationErrors, testing) => {
        let testingReport = ''
        const fs = require('fs')

        // The testing flag is used for unit testing so we can skip all the file I/O.
        // When testing, this function just returns a string with the same data that would've been written to a file
        const logger = testing 
            ? { write: (newLog) => testingReport = testingReport.concat(newLog), end: () => {} } 
            : fs.createWriteStream('migrationReport.txt')

        const missedRecordErrors = migrationErrors.filter(e => e.errorType == AccountErrorTypes.MISSED)
        const addedRecordErrors = migrationErrors.filter(e => e.errorType == AccountErrorTypes.ADDED)
        const corruptedRecordErrors = migrationErrors.filter(e => e.errorType == AccountErrorTypes.CORRUPTED)

        logger.write('REPORT SUMMARY:\n')
        logger.write(`\t${missedRecordErrors.length} records were missed during the migration.\n`)
        logger.write(`\t${addedRecordErrors.length} records were added since the migration.\n`)
        logger.write(`\t${corruptedRecordErrors.length} records were corrupted during the migration.\n\n`)

        logger.write('REPORT DETAILS:\n')
        logger.write('\tMISSED RECORDS:\n')
        missedRecordErrors.forEach(e => {
            logger.write(`\t\tRecord with ID: ${e.accountId} was missed during the migration.\n`)
        })
        logger.write('\tADDED RECORDS:\n')
        addedRecordErrors.forEach(e => {
            logger.write(`\t\tRecord with ID: ${e.accountId} was added since the migration.\n`)
        })
        logger.write('\tCORRUPTED RECORDS:\n')
        corruptedRecordErrors.forEach(e => {
            logger.write(`\t\tRecord with ID: ${e.accountId} was corrupted during the migration.\n`)
            e.corruptions.forEach(c => {
                logger.write(`\t\t\tCorrupted field: '${c.field}'\n\t\t\tPre-migration value: ${c.oldValue}\n\t\t\tPost-migration value: ${c.newValue}\n`)
            })
        })

        logger.end()
        return testing ? testingReport : null
    }
}