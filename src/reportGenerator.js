const { performance } = require('perf_hooks')
const { OldAccount, NewAccount } = require('./model/Account')
const { getMigrationErrors, generateMigrationReport } = require("./util/report.util")
const db = require("./schema")
const ora = require('ora')

db.old.sync()
db.new.sync()

const throbber = ora('Fetching pre/post-migration accounts...').start()

const oldAccountsPromise = new Promise((resolve, reject) => {
    db.oldAccounts.findAll()
        .then(accounts => {
            const preMigrationAccounts = accounts.map(a => new OldAccount(a.dataValues))
            throbber.stopAndPersist({ text: 'Pre-migration accounts fetched!' })
            resolve(preMigrationAccounts)
        })
        .catch(err => {
            throbber.stopAndPersist({ text: 'Error fetching pre-migration accounts. Is the docker image mounted?' })
            reject(err)
        })
        .finally(() => {
            db.old.close()
        })
})

const newAccountsPromise = new Promise((resolve, reject) => {
    db.newAccounts.findAll()
        .then(accounts => {
            const postMigrationAccounts = accounts.map(a => new NewAccount(a.dataValues))
            throbber.stopAndPersist({ text: 'Post-migration accounts fetched!' })
            resolve(postMigrationAccounts)
        })
        .catch(err => {
            throbber.stopAndPersist({ text: 'Error fetching post-migration accounts. Is the docker image mounted?' })
            reject(err)
        })
        .finally(() => {
            db.new.close()
        })
})

Promise.all([oldAccountsPromise, newAccountsPromise])
    .then((accounts) => {
        const [oldAccounts, newAccounts] = accounts

        const reportThrobber = ora('Generating Migration Error Report...').start()
        let timerStart = performance.now()

        const migrationErrors = getMigrationErrors(oldAccounts, newAccounts)
        generateMigrationReport(migrationErrors)

        let timerEnd = performance.now()
        reportThrobber.stopAndPersist({ text: 'Migration Error Report Generated!!' })

        console.log(`  Report Generation took: ${((timerEnd - timerStart)/1000).toFixed(2)} seconds`)
    })
