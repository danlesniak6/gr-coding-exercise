# README #

### How do I get set up? ###

* Mount the dockerized databases
* Install npm
* Navigate to root folder
* Run 'npm install'
* Run 'npm start' to generate the error report
* Generated report will be called 'migrationReport.txt' and will output to the root folder
* Run 'npm test' to run unit tests